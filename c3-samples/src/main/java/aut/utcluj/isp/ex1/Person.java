package aut.utcluj.isp.ex1;

/**
 * @author stefan
 */
public class Person {
    private String firstName;
    private String lastName;

    public Person(String firstName) {
        this.lastName = "";
        this.firstName = firstName;
    }

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }
    
    public boolean equals(Person p){
        if((this.firstName == p.firstName)&&(this.lastName==p.lastName))
            return true;
        else return false;
    }
    
   
    @Override
    public String toString(){
        return this.firstName + " "+ this.lastName;
    }
    
}