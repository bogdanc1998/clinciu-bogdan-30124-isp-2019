package aut.utcluj.isp.ex4;

/**
 * @author stefan
 */
public class NegativeBalanceException extends RuntimeException {
    int c;
      public NegativeBalanceException(int c,String msg) {
            super(msg);
            this.c = c;
      }
 
      int getConc(){
            return c;
      }
}
