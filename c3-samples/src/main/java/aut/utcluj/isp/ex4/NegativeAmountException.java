package aut.utcluj.isp.ex4;

/**
 * @author stefan
 */
public class NegativeAmountException extends RuntimeException {
        int c;
      public NegativeAmountException(int c,String msg) {
            super(msg);
            this.c = c;
      }
 
      int getConc(){
            return c;
}
}
