package aut.utcluj.isp.ex3;

/**
 * @author stefan
 */
public class Employee {
    private String firstName;
    private String lastName;
    private Double salary;
    private String cnp;
    
    public Employee(String firstName, String lastName, Double salary, String cnp) {
        this.cnp=cnp;
        this.firstName=firstName;
        this.lastName=lastName;
        this.salary=salary;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public double getSalary() {
        return salary;
    }
    
    public void setSalary(double salary)
    {
        this.salary=salary;
    }
    public String getCnp() {
        return cnp;
    }
}
