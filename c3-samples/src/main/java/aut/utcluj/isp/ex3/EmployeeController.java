package aut.utcluj.isp.ex3;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
;

/**
 * @author stefan
 */
public class EmployeeController {

    private ArrayList<Employee> emp = new ArrayList<>();

    public void addEmployee(final Employee employee) {
        emp.add(employee);
    }

    public Employee getEmployeeByCnp(final String cnp) {
     for (int i = 1; i < emp.size();i++)
        if(emp.get(i).getCnp()==cnp)
            return emp.get(i);
     return null;

    }

    /**
     * Update employee salary by cnp
     *
     * @param cnp - unique cnp
     * @param salary - salary
     * @return updated employee
     */
    public Employee updateEmployeeSalaryByCnp(final String cnp, final Double salary) {
         for (int i = 1; i < emp.size();i++)
         if(emp.get(i).getCnp()==cnp)
         {emp.get(i).setSalary(salary);
         return emp.get(i);}
         return null;
                
    }

    /**
     * Delete employee by cnp
     *
     * @param cnp - unique cnp
     * @return deleted employee or null if not found
     */
    public Employee deleteEmployeeByCnp(final String cnp) {
        for (int i = 1; i < emp.size();i++)
         if(emp.get(i).getCnp()==cnp)
         {   emp.remove(emp.get(i));
             return emp.get(i);       
         }
        return null;
          }
    

    /**
     * Return current list of employees
     *
     * @return current list of employees
     */
    public List<Employee> getEmployees() {
       return emp;
    }

    /**
     * Get number of employees
     *
     * @param o
     * @return - number of registered employees
     */
 
    public int getNumberOfEmployees() {
       return emp.size();
    }
    
    }
