package aut.utcluj.isp.ex2;

/**
 * @author stefan
 */
public class Employee extends Person{
    protected double salary;
    protected String firstName;
    protected String lastName;

    /**
     *
     * @param firstName
     * @param lastName
     * @param salary
     */
    Employee(String firstName, String lastName, double salary) {
        //throw new UnsupportedOperationException("Not supported yet.");
        this.firstName=firstName;
        this.lastName=lastName;
        this.salary=salary;
    }

    public Double getSalary() {
        return salary;
    }

    /**
     * Show employee information
     * @return employee information (Firstname: firstname Lastname: lastname Salary: salary)
     */
    public String showEmployeeInfo() {
        return "Firstname: "+ firstName + " Lastname: "+lastName+" Salary: "+salary;
    }
}
