package aut.utcluj.isp.ex2;

/**
 * @author stefan
 */
public class Person {
    protected String firstName;
    protected String lastName;

    public Person(){
        this.firstName="";
        this.lastName="";
    }
    
    public Person(String firstName) {
        this.firstName = firstName;
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    public Person(String firstName, String lastName) {
       // throw new UnsupportedOperationException("Not supported yet.");
       this.firstName = firstName;
       this.lastName=lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}