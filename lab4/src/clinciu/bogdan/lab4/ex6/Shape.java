/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab4.ex6;
/**
 *
 * @author Bogdan
 */
public class Shape {
    public String color;
    public boolean filled;
    
    
    public Shape(){
        this.setColor("red");
        this.setFilled(true);
      
    }
    
    public Shape(String color,boolean F ){
        this.setColor(color);
        this.setFilled(F);
    }
    
     public void setColor(String color){
        this.color=color;
    }
     
    public String getColor(){
        return this.color;
    }
     
    
    public void setFilled(boolean Fill){
        this.filled=Fill;
    }
    
    public boolean isFilled(){
        return this.filled;
    }
    
    @Override
    public String toString()
    {    if(filled)
       return "A Shape with color of " + color + " and filled.";
    else
        return "A Shape with color of " + color + " and not filled.";
    }
}

class Circle extends Shape
{
    public double radius;
    
    public Circle(){
        this.setRadius(1);
    }
    
    public Circle(double radius){
        this.setRadius(radius);
    }
    
    public Circle(double radi,String N,boolean filled){
        this.setRadius(radi);
        this.setColor(N);
        this.setFilled(filled);
    }
    
      public double getRadius(){
        return radius;
    }
      
    public void setRadius(double radius){
        this.radius=radius;
    }
    
    public double getArea(){
        return 3.14*radius*radius;
    }
    
    public double getPerimeter(){
        return 2*3.14*radius;
    }
    
    
   
    @Override
    public String toString()
    {
       return "A Circle with radius= " + radius + ", which is a subclass of " + super.toString();
    }
}

class Rectangle extends Shape
{
    public double length;
    public double width;

    
    public Rectangle(){
        this.length=1.0;
        this.width=1.0;
    }
    
    public Rectangle(double Len, double Wid){
        this.setLength(Len);
        this.setWidth(Wid);
    }
    
    public Rectangle(double LL,double WW,String N,boolean filled){
        this.setWidth(WW);
        this.setLength(LL);
        this.setColor(N);
        this.setFilled(filled);
        
    }
    
    public void setLength(double pb){
        this.length=pb;
    }
    
    public void setWidth(double cp){
        this.width=cp;
    }
    
    public double getLength(){
        return length;
    }
    
    public double getWidth(){
        return width;
    }
    
    public double getArea(){
        return width*length;
    }
    
    public double getPerimeter()
    {
        return (width+length)*2;
    }
    
    @Override
    public String toString()
    {
       return "A Rectangle with width = " + width + " and length = " + length + ", which is a subclass of " + super.toString();
    }
}

class Square extends Rectangle
{
   
    public Square(){
        
    }
    
    public Square(double side){
        this.setSide(side);
        
    }
    
    public Square(double Sid,String N,boolean filled){
        this.setSide(Sid);
        this.setColor(N);
        this.setFilled(filled);
        
    }
    
     public void setSide(double sidee)
     {
    this.length=sidee;
    this.length=sidee;
     }
     
    @Override
     public void setWidth(double side)
     {
         this.width=side;
     }
     
    @Override
     public void setLength(double side)
     {
         this.length=side;
     }
     
    @Override
     public String toString()
     {
     return "A Square with side = " + this.length + ", which is a subclass of " + super.toString();
    
     }
}



