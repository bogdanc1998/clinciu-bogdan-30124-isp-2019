/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab4.ex6;

/**
 *
 * @author Bogdan
 */
public class TestShape {
     public static void main(String args[])
     {
         Shape shape = new Shape("Blue",true);
         Circle circle = new Circle(23.5,"green",false);
         Rectangle Rect = new Rectangle(5.4,12.3,"magenta",true);
         Square Sqr = new Square(4.3,"grey",false);
         
         System.out.println(shape.toString());
         
        circle.setColor("orange");
        System.out.println(circle.toString());
        System.out.println("Area: " + circle.getArea() + "\n");
        System.out.println("Perimeter: " + circle.getPerimeter() + "\n");
        
        Rect.setWidth(8.7);
        Rect.setLength(11.2);
        System.out.println(Rect.toString());
        System.out.println("Area: " + Rect.getArea() + "\n");
        System.out.println("Perimeter: " + Rect.getPerimeter() + "\n");
        
        Sqr.setSide(5);
        System.out.println(Sqr.toString());
         
     }
}
