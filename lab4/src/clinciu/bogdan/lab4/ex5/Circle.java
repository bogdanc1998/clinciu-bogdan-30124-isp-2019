/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab4.ex5;

/**
 *
 * @author Bogdan
 */
public class Circle {
    public double radius;
    public String color;
    
    public Circle(){
        this.setRadius(1);
        this.setColor("red");
    }
    
    public Circle(double radius){
        this.setRadius(radius);
        this.setColor("red");
    }
    
    public void setRadius(double radius){
        this.radius=radius;
    }
    
    public void setColor(String color){
        this.color=color;
    }
    
    public double getRadius(){
        return radius;
    }
    
    public double getArea(){
        return 3.14*radius*radius;
    }
    
    @Override
    public String toString()
    {
       return "Radius= " + radius + "." + "\nColor= " + color + ".";
    }
}

class Cylinder extends Circle
{
    private double height=1.0;
    
    public Cylinder(){
        this.height=1.0;
    }
    
    public Cylinder(double radius){
        this.height=1.0;
        this.radius=radius;
    }
    
    public Cylinder(double radius, double height){
        this.radius=radius;
        this.height=height;
    }
    
    public double getHeight(){
        return this.height;
    }
    
    public double getVolume(){
        return this.height*3.1415926535*this.radius*this.radius;
    }
    
    @Override
    public double getArea(){
        return 2*3.1415926535*this.radius*(this.radius+this.height);
    }
}
    

