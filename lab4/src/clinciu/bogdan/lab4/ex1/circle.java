/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab4.ex1;

/**
 *
 * @author Bogdan
 */
public class circle {
    private double radius;
    private String color;
    
    public circle(){
        this.setRadius(1);
        this.setColor("red");
    }
    
    public void setRadius(double radius){
        this.radius=radius;
    }
    
    public void setColor(String color){
        this.color=color;
    }
    
    public double getRadius(){
        return radius;
    }
    
    public double getArea(){
        return 3.14*radius*radius;
    }
}
