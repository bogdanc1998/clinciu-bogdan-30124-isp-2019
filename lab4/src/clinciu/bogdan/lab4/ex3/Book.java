/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab4.ex3;
import clinciu.bogdan.lab4.ex2.*;
/**
 *
 * @author Bogdan
 */
public class Book {
    private String name;
    private Author author;
    private double price;
    private int qtyInStock=0;
    
    /**
     *
     * @param name
     * @param author
     * @param price
     */
    public Book(String name, Author author,double price)
    {
        this.name=name;
        this.author=author;
        this.price=price;
    }
    
     public Book(String name, Author author,double price,int q)
    {
        this.name=name;
        this.author=author;
        this.price=price;
        this.qtyInStock=q;  
    }
    
    public String getName()
    {
        return name;
    }
    
    public Author getAuthor()
    {   

        return this.author;
    }
    
    public double getPrice()
    {
        return this.price;
    }
    
    public void setPrice(double p)
    {
        this.price=p;
    }
    
    public int getqtyInStock()
    {
        return this.qtyInStock;
    }
    
    public void setqtyInStock(int q)
    {
        this.qtyInStock=q;
    }
   
    @Override
     public String toString()
     {
         return "'" + name + "' " + author + " ";
     }
    
}



    
    

