/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab4.ex4;
import clinciu.bogdan.lab4.ex2.*;
import java.util.Arrays;
/**
 *
 * @author Bogdan
 */
public class Book {
    private String name;
    private Author[] authors;
    private double price;
    private int qtyInStock=0;
    
    /**
     *
     * @param name
     * @param authors
     * @param price
     */
    public Book(String name, Author[] authors,double price)
    {
        this.name=name;
        this.authors=authors;
        this.price=price;
    }
    
     public Book(String name, Author[] author,double price,int q)
    {
        this.name=name;
        this.authors=author;
        this.price=price;
        this.qtyInStock=q;  
    }
    
    public String getName()
    {
        return name;
    }
    
    
    public Author[] getAuthors()
    {   
        return this.authors;
    }
    
    public double getPrice()
    {
        return this.price;
    }
    
    public void setPrice(double p)
    {
        this.price=p;
    }
    
    public int getqtyInStock()
    {
        return this.qtyInStock;
    }
    
    public void setqtyInStock(int qtyInStock)
    {
        this.qtyInStock=qtyInStock;
    }
  
    @Override
    public String toString()
     {
         return "'" + name + "' " + Arrays.toString(authors) + " ";
     }
    public void printAuthors()
    {
        System.out.println(Arrays.toString(authors) + " \n");
    }
}
