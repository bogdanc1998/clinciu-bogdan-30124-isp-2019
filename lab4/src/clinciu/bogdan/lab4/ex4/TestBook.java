/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab4.ex4;
import clinciu.bogdan.lab4.ex2.Author;
import java.util.Arrays;
/**
 *
 * @author Bogdan
 */
public class TestBook {
    
     public static void main(String[] args){
        
        Author aut[]=new Author[3];
        aut[0] = new Author("Vasile Pop","vpop@gmail.com",'m');
        aut[1] = new Author("Cristophor Columb","thisisamerica@gmail.com",'m');
        aut[2] = new Author("Benedict Cumberbatch","bettlefieldCounterstrike@gmail.com",'m');

        Book c = new Book("Algebra Liniara si Geometrie Analitica", aut , 50.0 , 500);
        System.out.println(aut[0]+"\n");
        System.out.println(c);
        aut[0].setEmail("vasilepop@utcn.ro");
        aut[1].setEmail("TheGotFather@yahoo.com");
        c.setPrice(123.24);
        c.setqtyInStock(1000);
        System.out.println(c.getName() + Arrays.toString(c.getAuthors()) + "  " + c.getPrice() + "  "+ c.getqtyInStock() + "\n");
        System.out.println(c+"\n");
        c.printAuthors();
}
}
