/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab7.ex2;

/**
 *
 * @author Bogdan
 */
import java.io.*;

public class CharacterTest {
    public static void main(String[] args) throws IOException {
        int counter;
        counter = 0;
        try {
            BufferedReader in = new BufferedReader(new FileReader("C:\\Users\\Bogdan\\Documents\\NetBeansProjects\\Test.txt"));
            String s = new String();
            BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Enter the character you want to count: ");
            char v = stdin.readLine().charAt(0);
            char uv = Character.toUpperCase(v);
            // System.out.println(stdin.readLine());
            while ((s = in.readLine()) != null) {
                String cuv = s.toUpperCase();
                for (int i = 0; i < cuv.length(); i++) {
                    char c = cuv.charAt(i);
                    if (uv == c) {
                        counter++;
                    }

                }

            }
            System.out.println("Your character appears " + counter + " times");

            in.close();
        } catch (
                IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
