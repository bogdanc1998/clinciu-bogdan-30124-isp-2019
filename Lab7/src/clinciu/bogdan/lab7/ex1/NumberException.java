/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab7.ex1;

/**
 *
 * @author Bogdan
 */
public class NumberException extends Exception {
      int n;
      public NumberException (int n,String msg) {
            super(msg);
            this.n = n;
      }
 
      int getNum(){
            return n;
      }
}