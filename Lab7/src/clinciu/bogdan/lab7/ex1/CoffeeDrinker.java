/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab7.ex1;

/**
 *
 * @author Bogdan
 */
class CofeeDrinker{
      void drinkCofee(Coffee c) throws TemperatureException, ConcentrationException{
            if(c.getTemp()>60)
                  throw new TemperatureException(c.getTemp(),"Cofee is to hot!");
            if(c.getConc()>50)
                  throw new ConcentrationException(c.getConc(),"Cofee concentration to high!");         
            System.out.println("Drink cofee:"+c);
      }
}//.class