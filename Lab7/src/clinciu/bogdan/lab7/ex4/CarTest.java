/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab7.ex4;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.Charset;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;

/**
 *
 * @author Bogdan
 */
public class CarTest {

    public static void main(String[] args) throws Exception {
        CarFactory kar = new CarFactory();
        Car a = kar.createCar("aaa", (int) (Math.random() * 1000));
        Car b = kar.createCar("bbb", (int) (Math.random() * 1000));
        Path path = Paths.get("C:\\Users\\Bogdan\\Documents\\NetBeansProjects\\Masini.txt");
        try {
            Files.createFile(path);
        } catch (FileAlreadyExistsException e) {
            System.err.println("already exists: " + e.getMessage());
        }

        kar.saveCar(a, "C:\\Users\\Bogdan\\Documents\\NetBeansProjects\\Masini.txt");
        kar.saveCar(b, "C:\\Users\\Bogdan\\Documents\\NetBeansProjects\\Masini.txt");

        kar.readCar(a, "C:\\Users\\Bogdan\\Documents\\NetBeansProjects\\Masini.txt");
        kar.readCar(b, "C:\\Users\\Bogdan\\Documents\\NetBeansProjects\\Masini.txt");

        kar.showCars("C:\\Users\\Bogdan\\Documents\\NetBeansProjects\\Masini.txt");

    }
}
