/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab7.ex4;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Bogdan
 */

public class FilesReaderWriter {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = null;
        PrintWriter outputStream = null;
        ArrayList<String> rows = new ArrayList<String>();

        try {
            reader = new BufferedReader(new FileReader("C:\\Users\\Bogdan\\Documents\\NetBeansProjects\\Masini.txt"));
            outputStream = new PrintWriter(new FileWriter("C:\\Users\\Bogdan\\Documents\\NetBeansProjects\\Masini.txt"));

            String file;
            while ((file = reader.readLine()) != null) {
                rows.add(file);
            }
            Collections.sort(rows);
            String[] strArr = rows.toArray(new String[0]);
            for (String cur : strArr) {
                outputStream.println(cur);
            }
        } finally {
            if (reader != null) {
                outputStream.close();
            }
            if (outputStream != null) {
                outputStream.close();
            }
        }
    }

}
