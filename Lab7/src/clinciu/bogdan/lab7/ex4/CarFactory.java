/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab7.ex4;

import java.io.*;

/**
 *
 * @author Bogdan
 */

public class CarFactory {

    Car createCar(String model, double price) {
        Car m = new Car(model, price);
        //System.out.println(model + " costs " + price);
        return m;
    }

    void showCars(String storeRecipientData) throws IOException {
        BufferedReader in = new BufferedReader(new FileReader(storeRecipientData));
        String h;
        while ((h = in.readLine()) != null) {
            System.out.println(h);
        }
    }

    void readCar(Car m, String storeRecipientData) throws IOException, ClassNotFoundException {
        BufferedReader in = new BufferedReader(new FileReader(storeRecipientData));
        String h;
        boolean ok = false;
        while ((h = in.readLine()) != null) {
            if (h.equals(m.toString()) && ok == false) {
                System.out.println("Modelul cautat a fost gasit " + m + ".");
                ok = true;
            }
        }
        if (ok == false) {
            System.out.println("Modelul " + m + " nu a fost gasit.");
        }
    }

    void saveCar(Car m, String storeRecipientName) throws IOException {
        String fileContent = m.toString();
        BufferedWriter writer = new BufferedWriter(new FileWriter(storeRecipientName, true));
        writer.newLine();
        writer.write(m.id + ". " + fileContent);
        writer.close();
    }
}
