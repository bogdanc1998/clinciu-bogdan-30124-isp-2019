/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab2.ex6;
import java.util.Scanner;

/**
 *
 * @author Bogdan
 */
public class Factorial {
    static int factorialr(int n){
        if(n==0) return 1;
        return (n*factorialr(n-1));
    }
    
    static void factoriali(int n){
        int d=1;
        if(n==0) System.out.println("n!= "+"1");
        else{
            for(int i=1;i<=n;++i){
                d*=i;
            }
            System.out.println(d);
        }
    }
    
    public static void main(String[] args){
        Scanner keyboard=new Scanner(System.in);
        System.out.println("Introduceti un numar:");
        int f=keyboard.nextInt();
        System.out.println("1.Recursiv");
        System.out.println("2.Iterativ");
        int n=keyboard.nextInt();
        switch(n){
            case 1: 
                System.out.println(factorialr(f));
                break;
            case 2:
                factoriali(f);
                break;
            default:
                System.out.println("Metoda introdusa nu exista");
        }
    }
}

