/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab2.ex5;
import java.util.Random;
/**
 *
 * @author Bogdan
 */
public class BubbleSort {
    
    static void afisare(int v[], int n){
        for(int i=1;i<=n;i++){
            System.out.print(v[i]);
            System.out.print(" ");
        }
        System.out.print('\n');
    }
    
    static void bubblesort(int v[], int n){
        int i, j, aux; 
        boolean ok; 
        for (i = 1; i < n; i++)  
        { 
            ok = false; 
            for (j = 1; j < n - i ; j++)  
            { 
                if (v[j] > v[j + 1])  
                { 
                    aux = v[j]; 
                    v[j] = v[j + 1]; 
                    v[j + 1] = aux; 
                    ok = true; 
                } 
            }
            if (ok == false) 
                break; 
        } 
    }
    
    public static void main(String[] args){
        Random rand= new Random();
        int[] v= new int[11];
        for(int i=1;i<=10;i++){
            v[i]=rand.nextInt(51);
        }
        bubblesort(v,11);
        afisare(v, 10);
    }
}

