/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab2.ex4;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Bogdan
 */
public class MaximVector {
    
    public static void main(String args[]){
        Scanner in = new Scanner(System.in);
        System.out.println("Nr elemente:");
        int n = in.nextInt();
        int[] x;
        x = new int[n];
        int max= x[0];
        for(int i=1;i<n;i++)
        {
        if(x[i]>max)
            max=x[i];
        }
        
        System.out.println("Maximul este:"+max);
    }

}
