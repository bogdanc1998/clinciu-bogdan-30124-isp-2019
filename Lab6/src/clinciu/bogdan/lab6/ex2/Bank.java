/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab6.ex2;

import java.util.ArrayList;
import java.util.Comparator;

/**
 *
 * @author Bogdan
 */

public class Bank {

    private ArrayList<BankAccount> al = new ArrayList<>();

    void printAccounts() {
        BalanceCompare balanceCompare = new BalanceCompare();
        al.sort(balanceCompare);
        for (BankAccount o : al) {
            System.out.println(o);
        }
    }

    public void addAccount(String owner, double balance) {
        al.add(new BankAccount(owner, balance));
    }

    public void printAccounts(double minBalance, double maxBalance) {
        for (BankAccount o : al) {
            if ((o.getBalance() > minBalance) && (o.getBalance() < maxBalance)) {
                System.out.println(o);
            }
        }
    }

    BankAccount getOneAccount(int i){
        return al.get(i);
    }
    
    public void getAllAccounts() {
        OwnerCompare ownerCompare = new OwnerCompare();
        al.sort(ownerCompare);
        for (BankAccount o : al) {
            System.out.println(o);
        }
    }
}

class OwnerCompare implements Comparator<BankAccount> {

    @Override
    public int compare(BankAccount owner1, BankAccount owner2) {
        return owner1.getOwner().compareTo(owner2.getOwner());

    }
}

class BalanceCompare implements Comparator<BankAccount> {

    public int compare(BankAccount owner1, BankAccount owner2) {
        return Double.compare(owner1.getBalance(), owner2.getBalance());
    }
}
