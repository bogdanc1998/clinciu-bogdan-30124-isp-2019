/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab6.ex2;

import java.util.Objects;

/**
 *
 * @author Bogdan
 */
public class BankAccount {

    public String owner;
    public double balance;

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public BankAccount(String owner) {
        this.owner = owner;
    }

    public String getOwner() {
        return this.owner;
    }

    public double getBalance() {
        return this.balance;
    }

    public void withdraw(double ammount) {
        if (this.balance == 0) {
            System.out.println("Balance 0");
        } else if (ammount > this.balance) {
            System.out.println("Insufficient funds");
        } else {
            this.balance -= ammount;
        }
    }

    public void deposit(double ammount) {
        this.balance += ammount;    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BankAccount that = (BankAccount) o;
        return owner.equals(that.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.owner);
    }

    @Override
    public String toString() {
        return "BankAccount{"
                + "name='" + owner + '\''
                + ", balance=" + balance
                + '}';
    }
}
