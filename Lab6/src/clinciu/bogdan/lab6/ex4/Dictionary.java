/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab6.ex4;

/**
 *
 * @author Bogdan
 */
import java.util.HashMap;

public class Dictionary {
    HashMap<String, String> dictionary = new HashMap<>();

    public void addWord(String Word, String Definition) {
        dictionary.put(Word, Definition);
    }

    public String getDefinition(String Word) {
        return dictionary.get(Word);
    }

    public void getAllDefinitions() {
        for (String i : dictionary.values()) System.out.println(i);
    }

    public void getAllWords() {
        for (String i : dictionary.keySet()) System.out.println(i);
    }


}

