/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab6.ex4;

/**
 *
 * @author Bogdan
 */
import java.util.Scanner;

public class ConsoleMenu {

    public static void main(String[] args) {
        Dictionary minidex = new Dictionary();
        minidex.addWord("Prototip", "Model care reprezintă tipul original după care se efectuează sau se realizează ceva; prim exemplar de probă dintr-un lot de piese, de mașini etc. care urmează să se execute în serie.");
        minidex.addWord("Antierou", "Personaj negativ al unei opere literare; erou negativ");
        minidex.addWord("Anecdota", "storioară hazlie și scurtă, de multe ori batjocoritoare");
        minidex.addWord("Antidot", "Substanță care neutralizează acțiunea unei otrăvi, a unui virus etc. din organism");
        minidex.addWord("Cascaval", "Specie de brânză fină, tare, în formă de turte sau de roți, preparată din caș de lapte de oaie");

        System.out.println("Choose one: ");
        System.out.println("1. Afisarea tuturor cuvintelor");
        System.out.println("2. Afisarea tuturor definitiilor");
        System.out.println("3. Afisarea definitiei unui cuvant dat");

        Scanner keyboard = new Scanner(System.in);
        int option = keyboard.nextInt();

        switch (option) {
            case 1:
                minidex.getAllWords();
                break;
            case 2:
                minidex.getAllDefinitions();
                break;
            case 3:
                System.out.println("Introduceti cuvantul dorit: ");
                String option2 = keyboard.next();
                System.out.println(minidex.getDefinition(option2));
                break;
        }
    }
}
