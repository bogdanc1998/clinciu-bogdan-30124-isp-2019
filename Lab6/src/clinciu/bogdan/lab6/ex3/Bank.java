/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab6.ex3;


import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;
/**
 *
 * @author Bogdan
 */
public class Bank {
    private TreeSet<BankAccount> treeSet = new TreeSet<>();

    public void addAccount(String owner, double balance) {
        treeSet.add(new BankAccount(owner, balance));
    }

    public void printAccounts() {
        SortedSet<BankAccount> bankAccountSortedSet;
        bankAccountSortedSet = treeSet;
        for (BankAccount o : bankAccountSortedSet) {
            System.out.println(o);
        }
    }

    public void printAccounts(double minBalance, double maxBalance) {
        for (BankAccount o : treeSet)
            if ((o.getBalance() > minBalance) && (o.getBalance() < maxBalance)) {
                System.out.println(o);
            }
    }

    public void getAllAccounts() {
        SortedSet<BankAccount> bankAccounts = new TreeSet<>(Comparator.comparing(BankAccount::getOwner));
        for(BankAccount o:treeSet){
            bankAccounts.add(o);
        }
        for (BankAccount o : bankAccounts) {
            System.out.println(o);
        }
    }
}