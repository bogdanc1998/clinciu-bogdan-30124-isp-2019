/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab5.ex3;

import java.util.Random;

/**
 *
 * @author Bogdan
 */
public class LightSensor extends Sensor {

    int light;

    public void setLight(int light) {
        this.light = light;
    }

    LightSensor() {
        Random Lum = new Random();
        this.light = Lum.nextInt(100);
    }

    public int readValue() {
        return this.light;
    }
}
