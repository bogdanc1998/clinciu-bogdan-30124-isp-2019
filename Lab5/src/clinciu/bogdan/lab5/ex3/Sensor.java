/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab5.ex3;

/**
 *
 * @author Bogdan
 */
public abstract class Sensor {

    private String location;

    public void Sensor(String S) {
        this.location = S;
    }

    public abstract int readValue();

    public String getLocation() {
        return this.location;
    }
}
