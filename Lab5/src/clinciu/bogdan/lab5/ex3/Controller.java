/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab5.ex3;

/**
 *
 * @author Bogdan
 */
class Controller {

    Sensor temp;
    Sensor light;

    public void control(int a, int b) {
        for (int i = 0; i < b; i += a) {
            this.temp = new TemperatureSensor();
            this.light = new LightSensor();
            System.out.println("Temperature: " + Integer.toString(temp.readValue()) + " Light: " + Integer.toString(light.readValue()));
        }
    }

}
