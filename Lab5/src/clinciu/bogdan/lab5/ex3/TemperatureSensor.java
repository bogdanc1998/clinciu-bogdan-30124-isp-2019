/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab5.ex3;

import java.util.Random;

/**
 *
 * @author Bogdan
 */
public class TemperatureSensor extends Sensor {

    int temp;

    public void setTemperature(int temp) {
        this.temp = temp;
    }

    TemperatureSensor() {
        Random tmp = new Random();
        this.temp = tmp.nextInt(100);
    }

    public int readValue() {
        return this.temp;
    }
}
