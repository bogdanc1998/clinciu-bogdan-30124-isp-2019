/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab5.ex2;

public class RotatedImage implements Image {

    private String fileName;

    RotatedImage(String fileName) {
        this.fileName = fileName;
    }

    RotatedImage() {
        this.fileName = "Invers";
    }

    @Override
    public void display() {
        System.out.println("Display rotated " + fileName);
    }

}
