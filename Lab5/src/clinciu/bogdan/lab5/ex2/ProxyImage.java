/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab5.ex2;

public class ProxyImage implements Image {

    private Image image;
    private String fileName;

    public ProxyImage(String fileName, int ok) {
        this.fileName = fileName;
        if (ok == 1) {
            image = new RealImage(fileName);
        } else {
            image = new RotatedImage(fileName);
        }
    }

    @Override
    public void display() {
        image.display();
    }
}
