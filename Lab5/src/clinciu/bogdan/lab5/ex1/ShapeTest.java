/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab5.ex1;

/**
 *
 * @author Bogdan
 */
public class ShapeTest {

    public static void main(String args[]) {
        Circle circle = new Circle(23.5, "green", false);
        Rectangle Rect = new Rectangle(5.4, 12.3, "magenta", true);
        Square Sqr = new Square(4.3, "grey", false);

        circle.setColor("orange");
        System.out.println(circle.toString());
        System.out.println("Area: " + circle.getArea() + "\n");
        System.out.println("Perimeter: " + circle.getPerimeter() + "\n");

        Rect.setWidth(8.7);
        Rect.setLength(11.2);
        System.out.println(Rect.toString());
        System.out.println("Area: " + Rect.getArea() + "\n");
        System.out.println("Perimeter: " + Rect.getPerimeter() + "\n");

        Sqr.setSide(5);
        System.out.println(Sqr.toString());
    }
}
