/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab8.ex4;

/**
 *
 * @author Bogdan
 */
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

abstract class Home {
    private Random r = new Random();
    private final int SIMULATION_STEPS = 20;

    protected abstract void setValueInEnvironment(Event event, int nr) throws IOException;

    protected abstract void controlStep() throws IOException;

    protected abstract boolean gsmUnitAlert(Event event) throws IOException;

    protected abstract void heatingUnit(Event event) throws IOException;

    protected abstract void coolingUnit(Event event) throws IOException;

    private Event getHomeEvent() {
        //randomly generate a new event;
        int k = r.nextInt(100);
        if (k < 18)
            return new ColdEvent(r.nextBoolean());
        else if (k < 40 && k > 24)
            return new HotEvent(r.nextBoolean());
        else if (k < 60)
            return new FireEvent(r.nextBoolean());
        else
            return new TemperatureEvent(r.nextInt(50));
    }

    public void simulate() throws IOException {
        int k = 0;
        while (k < SIMULATION_STEPS) {
            Event event = this.getHomeEvent();
            System.out.print(k + ". ");
            setValueInEnvironment(event, k);
            controlStep();
            if(gsmUnitAlert(event)==true) return;
            heatingUnit(event);
            coolingUnit(event);
            try {
                Thread.sleep(300);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
            if(k==SIMULATION_STEPS){
                BufferedWriter writer = new BufferedWriter((new FileWriter("C:/Users/Tudor Banciu/Desktop/system_logs.txt", true)));
                writer.newLine();
                writer.close();
            }
            k++;
        }
    }
}
