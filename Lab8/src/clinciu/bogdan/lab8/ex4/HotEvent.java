/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab8.ex4;

/**
 *
 * @author Bogdan
 */
public class HotEvent extends Event {
    private boolean hot;

    HotEvent(boolean hot) {
        super(EventType.HOT);
        this.hot = hot;
    }

    boolean isHot() {
        return hot;
    }

    @Override
    public String toString() {
        return "HotEvent{cold=" + hot + "}";
    }

}
