/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab8.ex4;

/**
 *
 * @author Bogdan
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class HomeAutomation {

    public static void main(String[] args) throws IOException {
        //test using an anonymous inner class
        Home h = new Home() {

            protected void setValueInEnvironment(Event event, int nr) throws IOException {
                System.out.println("New event in environment " + event);
                String fileContent = nr + ". New event in environment " + event;
                BufferedWriter writer = new BufferedWriter(new FileWriter("C:\\Users\\Bogdan\\Documents\\NetBeansProjects\\SystemLogs.txt", true));
                writer.newLine();
                writer.write(fileContent);
                writer.close();
            }

            @Override
            protected void controlStep() throws IOException {
                System.out.println("Control step executed");
                String fileContent;
                fileContent = "Control step executed\n";
                BufferedWriter writer = new BufferedWriter(new FileWriter("C:\\Users\\Bogdan\\Documents\\NetBeansProjects\\SystemLogs.txt", true));
                writer.newLine();
                writer.write(fileContent);
                writer.close();
            }

            protected boolean gsmUnitAlert(Event event) throws IOException {
                if (event.type.equals(EventType.FIRE)) {
                    System.out.println("FIRE!");
                    String fileContent;
                    fileContent = "FIRE!";
                    BufferedWriter writer = new BufferedWriter(new FileWriter("C:\\Users\\Bogdan\\Documents\\NetBeansProjects\\SystemLogs.txt", true));
                    writer.newLine();
                    writer.write(fileContent+"\n");
                    writer.close();
                    return true;
                }
                return false;
            }

            protected void heatingUnit(Event event) throws IOException {
                if (event.type.equals(EventType.COLD)) {
                    System.out.println("The heating unit has been turned on.");
                    String fileContent = "The heating unit has been turned on.";
                    BufferedWriter writer = new BufferedWriter((new FileWriter("C:\\Users\\Bogdan\\Documents\\NetBeansProjects\\SystemLogs.txt", true)));
                    writer.newLine();
                    writer.write(fileContent);
                    writer.close();
                }
            }

            protected void coolingUnit(Event event) throws IOException {
                if(event.type.equals(EventType.HOT)){
                    System.out.println("The cooling unit has been turned on.");
                    String fileContent = "The cooling unit has been turned on.";
                    BufferedWriter writer = new BufferedWriter((new FileWriter("C:\\Users\\Bogdan\\Documents\\NetBeansProjects\\SystemLogs.txt", true)));
                    writer.newLine();
                    writer.write(fileContent);
                    writer.close();
                }
            }
        };

        /*BufferedWriter writer = Files.newBufferedWriter(Paths.get("C:/Users/Tudor Banciu/Desktop/system_logs.txt"));
        writer.write("");
        writer.flush();*/

        h.simulate();
    }
}