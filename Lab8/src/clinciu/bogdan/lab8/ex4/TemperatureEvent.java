/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab8.ex4;

/**
 *
 * @author Bogdan
 */
public class TemperatureEvent extends Event {
    private int temp;

    TemperatureEvent(int value) {
        super(EventType.FIRE.TEMPERATURE);
        this.temp = value;
    }

    int getvalue() {
        return temp;
    }

    @Override
    public String toString() {
        return String.format("TemperatureEvent{value=%d}", temp);
    }
}