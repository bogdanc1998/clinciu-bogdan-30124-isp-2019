package ro.utcluj.aut.isp.vehicles2;

import java.util.ArrayList;
import java.util.Comparator;


public class Parking {
ArrayList<Vehicle> parkedVehicles=new ArrayList<>();
   
public void parkVehicle(Vehicle e){
        parkedVehicles.add(e);
    }

    public void sortByWeight(){
        WeightCompare weightCompare=new WeightCompare();
        parkedVehicles.sort(weightCompare);
    }
   

    public Vehicle get(int index){
        return null;
    }

}

class WeightCompare implements Comparator<Vehicle>{
    @Override
    public int compare(Vehicle v1, Vehicle v2){
        return Integer.compare(v1.getWeight(),v2.getWeight());
    }
}
