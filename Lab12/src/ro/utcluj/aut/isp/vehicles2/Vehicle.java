package ro.utcluj.aut.isp.vehicles2;

import java.util.Objects;

public class Vehicle {

    private String type;
    private int weight;

    public Vehicle(String type, int weight) {
        this.type = type;
        this.weight=weight;
    }
    public int getWeight(){
        return this.weight;
    }

    public String start(){
        return "engine started";
    }
 @Override
    public boolean equals(Object o) {
       
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vehicle that = (Vehicle) o;
        return this.type==that.type && this.weight==that.weight;
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, weight);
    }
}

