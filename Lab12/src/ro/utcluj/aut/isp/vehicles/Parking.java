package ro.utcluj.aut.isp.vehicles;

import java.util.ArrayList;
import java.util.Comparator;


public class Parking {

    /**
     * Vehicles will be parked in parkedVehicles array.
     */
    ArrayList<Vehicle> parkedVehicles=new ArrayList<>();

    public void parkVehicle(Vehicle e){
           parkedVehicles.add(e);
    }

    /**
     * Sort vehicles by length.
     */
    public void sortByWeight(){
        WeightCompare weightCompare=new WeightCompare();
        parkedVehicles.sort(weightCompare);
    }

    public Vehicle get(int index){
        return parkedVehicles.get(index);
    }

}
class WeightCompare implements Comparator<Vehicle>{
    @Override
    public int compare(Vehicle v1, Vehicle v2){
        return Integer.compare(v1.getWeight(),v2.getWeight());
    }
}
