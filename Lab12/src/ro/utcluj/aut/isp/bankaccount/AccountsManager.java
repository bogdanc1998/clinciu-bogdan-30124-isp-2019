package ro.utcluj.aut.isp.bankaccount;

import java.util.ArrayList;

public class AccountsManager {

    private final ArrayList<BankAccount> l = new ArrayList<>();
    
    public void addAccount(BankAccount account){
        l.add(account);
    }

    public boolean exists(String id){
        for(BankAccount b:l){
            if(b.id==id){
                return true;
            }
        }
        return false;
    }

    public int count(){
            return l.size();
    }
}
