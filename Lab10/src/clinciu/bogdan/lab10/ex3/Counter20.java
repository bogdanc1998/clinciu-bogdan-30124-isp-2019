/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab10.ex3;

/**
 *
 * @author Bogdan
 */
class Counter20 extends Thread
{
      String n;
      Thread t;
      Counter20(String n, Thread t){this.n = n;this.t=t;}
 
      public void run()
      {int n1=0,n2=100,x=1;
    	          try
            {                
                  if (t!=null) { t.join();n1=100;n2=200;x=2;}
                  for(int i=n1;i<=n2;i++){
                      System.out.println(" i = "+i);}
                  Thread.sleep((int)(Math.random() * 1000));
                  System.out.println("Counter "+x+" finished it's job.");
                  
            }
            catch(Exception e){e.printStackTrace();} 
    	  }
 
      
 
public static void main(String[] args)
{
	Counter20 w1= new Counter20("Counter 1",null);
	Counter20 w2 = new Counter20("Counter 2",w1);
      w1.start();
      w2.start();
}
}