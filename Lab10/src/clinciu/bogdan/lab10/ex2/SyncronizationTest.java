/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab10.ex2;

/**
 *
 * @author Bogdan
 */
public class SyncronizationTest {
public static void main(String[] args) {
    Punct p = new Punct();
    FireSet fs1 = new FireSet(p);
    FireGet fg1 = new FireGet(p);
 
    fs1.start();
    fg1.start();
}
}
 
class FireGet extends Thread {
    Punct p;
 
    public FireGet(Punct p){
        this.p = p;
    }
 
    public void run(){
        int i=0;
        int a,b;
        while(++i<15){         
            synchronized(p){
            a= p.getX();          
            try {
                sleep(50);
            } catch (InterruptedException e) {  
                e.printStackTrace();
            }         
            b = p.getY();
            }
            System.out.println("Am citit: ["+a+","+b+"]");
        }
    }
}//.class
 
 
class FireSet extends Thread {
    Punct p;
    public FireSet(Punct p){
        this.p = p;
    } 
    public void run(){
        int i =0;
        while(++i<15){
            int a = (int)Math.round(10*Math.random()+10);
            int b = (int)Math.round(10*Math.random()+10);
 
            synchronized(p){
            p.setXY(a,b);
             }
 
            try {
                sleep(10);
            } catch (InterruptedException e) {
 
                e.printStackTrace();
            }
            System.out.println("Am scris: ["+a+","+b+"]");
        }
    }
}//.class
 
class Punct {
    int x,y;
    public void setXY(int a,int b){
        x = a;y = b;
    }  
    public int getX(){return x;}
    public int getY(){return y;}   
}