/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab3.ex4;
/**
 *
 * @author Bogdan
 */
public class MyPoint {
    private int x;
    private int y;
    
    public MyPoint(){}
    
    public MyPoint(int a, int b){
        this.x=a;
        this.y=b;
    }
    
    public int getX(int x){
        return x;
    }
    
    public int getY(int y){
        return y;
    }
    
    public void setX(int cs){
        this.x=cs;
    }
    
    public void setY(int igr){
        this.y=igr;
    }
    
    public void setXY(int c, int d){
        this.x=c;
        this.y=d;
    }
    
    @Override
    public String toString(){
        return "("+x+","+y+")";
    }
    
    public double distancion(int x, int y){
        double k;
        k = Math.sqrt((this.x-x)*(this.x-x) + (this.y-y)*(this.y-y));
        return k; 
    }
    
    public double distancion(MyPoint another){
       double j= Math.sqrt((another.x-this.x)*(another.x-this.x) + (another.y-this.y)*(another.y-this.y));
        return j;
    }
    
}