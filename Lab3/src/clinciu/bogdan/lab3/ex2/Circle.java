/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab3.ex2;

/**
 *
 * @author Bogdan
 */

    /**
     *
     * @param color
     */
public class Circle {
    private double radius;
    private String color;
    
    public Circle(){
        this.setRadius(1);
        this.setColor("red");
    }
    
    public void setRadius(double radius){
        this.radius=radius;
    }
    
    public void setColor(String color){
        this.color=color;
    }
    
    public double getRadius(){
        return radius;
    }
    
    public double getArea(){
        return 3.14*radius*radius;
    }
    
    @Override
    public String toString(){
        return "Radius= " + radius + "." + "\nColor= " + color + ".";
    }
}
