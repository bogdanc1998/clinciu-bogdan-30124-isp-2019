/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab3.ex1;

/**
 *
 * @author Bogdan
 */
public class Robot {
    private int x;

    public Robot(int value) {
        this.x = value;
    }

    public int getValue() {
        return x;
    }

    public void setValue(int k) {
        this.x = x+k;
    }

    @Override
    public String toString() {
        return "Robot{" +
                "Pozitia=" + x +
                '}';
    }
}
