/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab9.ex3;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

/**
 *
 * @author Bogdan
 */

public class FilePrinter extends JFrame {

    JLabel label;
    JTextArea textArea;
    JButton button;
    JTextField textField;

    public String fileReader(String f) throws IOException {
        String s = new String ( );
        try (BufferedReader reader = new BufferedReader (new FileReader (f))) {
            String a;
            while ((a = reader.readLine ( )) != null) {
                s += (a + '\n');
            }
        }
        return s;
    }

    FilePrinter() {
        setTitle ("Test login");
        setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
        init ( );
        setSize (640, 960);
        setVisible (true);
    }

    public void init() {
        this.setLayout (null);

        label = new JLabel ("Numele fisierului");
        label.setBounds (10, 50, 200, 100);

        textArea = new JTextArea ( );
        textArea.setBounds (10, 400, 300, 500);

        button = new JButton ("Afisare");
        button.setBounds (70, 260, 80, 20);
        button.addActionListener (new butonAfisare ( ));

        textField = new JTextField ( );
        textField.setBounds (10, 150, 300, 20);

        add (label);
        add (button);
        add (textArea);
        add (textField);
    }

    class butonAfisare implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            String fileName = FilePrinter.this.textField.getText ( );
            try {
                FilePrinter.this.textArea.append (fileReader (fileName));
            } catch (IOException e1) {
                e1.printStackTrace ( );
            }
        }
    }


    public static void main(String[] args) {
        new FilePrinter ( );
    }
}