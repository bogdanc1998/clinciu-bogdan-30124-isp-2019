/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab9.ex4;

/**
 *
 * @author Bogdan
 */
public class Segment {
    int id;
    Train train;

    Segment(int id) {
        this.id = id;
    }

    boolean hasTrain() {
        return train != null;
    }

    Train departTRain() {
        Train tmp = train;
        this.train = null;
        return tmp;
    }

    void arriveTrain(Train t) {
        train = t;
    }

    Train getTrain() {
        return train;
    }
}
