/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab9.ex2;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.util.*;

/**
 *
 * @author Bogdan
 */

public class CounterButton extends JFrame {

    public static int cc = 0;
    JButton bCounter;
    JTextField tText;

    CounterButton(int cc) {
        setTitle("Counter");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(500, 500);
        setVisible(true);
        init();
        tText.setText(Integer.toString(cc));
    }

    public void init() {
        this.setLayout(null);
        int width = 80;
        int height = 20;
        tText = new JTextField();
        tText.setBounds(70, 50, width, height);
        bCounter = new JButton("Incrementare");
        bCounter.setBounds(10, 150, 200, height);
        bCounter.addActionListener(new ButonCounter());
        add(bCounter);
        add(tText);
    }

    public static void main(String[] args) {
        new CounterButton(cc);
    }

    class ButonCounter implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            cc++;
            tText.setText(Integer.toString(cc));
        }
    }
}
