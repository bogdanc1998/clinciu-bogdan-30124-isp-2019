/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab11.ex2;

import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Label;
import java.awt.List;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author Bogdan
 */



public class ProductView extends JFrame {

	JButton bListProducts; 
	JButton bAddProduct;
	JButton bDeleteProduct;
	JButton bChangeQuantity;
	JTextArea content;
	ProductView(){
        setTitle("My Products");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();l
        setSize(500,650);
        setVisible(true);
  }
	public void init(){
		 
        this.setLayout(null);
        int width=130; int height = 40;
        bAddProduct = new JButton("Add Product");
        bAddProduct.setBounds(345,40,width, height);
        bAddProduct.addActionListener(new TratareButonAdd());
        bListProducts = new JButton("List Products");
        bListProducts.setBounds(345,90,width, height);
        bListProducts.addActionListener(new TratareButonList());
        bDeleteProduct= new JButton("Delete");
        bDeleteProduct.setBounds(345,140,width, height);
        bDeleteProduct.addActionListener(new TratareButonList());
        bChangeQuantity= new JButton("Change Quantity");
        bChangeQuantity.setBounds(345,190,width, height);
        bChangeQuantity.addActionListener(new TratareButonList());
        
        width=200; height = 30;
        content = new JTextArea();
        content.setBounds(60,40,270,400);
        add(bListProducts);
        add(bAddProduct);
        add(bDeleteProduct);
        add(bChangeQuantity);
        add(content);

  }
	
	public static void main(String[] args) {
		 ArrayList<Product> prod = new ArrayList<Product>();
		 new ProductView();
  }
	public void changelist() {
		if(bAddProduct.isSelected()) {
			String n;
			int q;
			double p;
			n=ProductView.this.content.getText(); 
			q=0;
			p=0;
			Product x;
			
			prod.add(new Product(n,q,p));
		}
	}
	
	class TratareButonList implements ActionListener{
			 
            public void actionPerformed(ActionEvent e) {
            	  add(content);
            	  content.setText("");
            	  String filename="C:\\Users\\Documents\\NetBeansProjects\\product.txt";
                  String encoding = "Cp1250";
                  File file1 = new File(filename);
                  if (file1.exists()) {
                      try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file1), encoding))) {
                          String line = null;
                          int k=0;
                          ProductView.this.content.append("Product"+"\t  "+"Quantity"+"\t  "+"Price"+"\n");
                          while ((line = br.readLine()) != null) {
                        	  if(k%3==0)
                        		  ProductView.this.content.append("\n");
                        	  ProductView.this.content.append(line+"\t  ");
                        	  k++;
                          }
                      } catch (IOException e1) {
                          e1.printStackTrace();
                      }
                  }
                  else {
                	  		ProductView.this.content.append("List empty");
                  }
                  
            }
          
	}
	class TratareButonAdd implements ActionListener{
		 public void actionPerformed(ActionEvent e) { 
			 content.setText("");
			 
		 }
	}
}