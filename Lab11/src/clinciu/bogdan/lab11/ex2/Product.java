/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab11.ex2;

/**
 *
 * @author Bogdan
 */
public class Product {
	private String name;
	private int quantity;
	private double price;
	
        Product(String n, int q, int p)
        {
            this.name=n;
            this.price=p;
            this.quantity=q;
        }
	public String getName() {
		return name;
	}
	public void setName() {
		this.name=name;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity() {
		this.quantity=quantity;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice() {
		this.price=price;
	}
	public boolean equals(Object o) {

        if (o == this) return true;
        if (!(o instanceof Product)) {
            return false;
        }

        Product x = (Product) o;

        return x.name.equals(name);
    }
	public final int hashCode(){
		return (int) (name.hashCode());
    }
	
}