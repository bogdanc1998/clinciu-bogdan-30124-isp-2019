/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab11.ex1;

import java.util.Observable;
import java.util.Random;

/**
 *
 * @author Bogdan
 */

public class Senzor extends Observable implements Runnable {
    private double tm = 0;
    private Thread t;
    private Random r = new Random();

    public void start() {
        if (t == null) {
            t = new Thread(this);
            t.start();
        }
    }

    public void run() {
        while (true) {
            double d = r.nextDouble() * 8 + 21;
            tm = d;
            this.setChanged();
            this.notifyObservers();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
        }
    }


    public double getTemperature() {
        return tm;
    }
}