/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab11.ex1;

import java.awt.FlowLayout;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Bogdan
 */
public class TextViewer extends JPanel implements Observer {

    JTextField tf;
    JLabel l;

    TextViewer() {
        this.setLayout(new FlowLayout());
        tf = new JTextField(10);
        l = new JLabel("Temperature:");
        add(tf);
        add(l);
    }

    public void update(Observable o, Object arg) {
        String s = "" + ((Senzor) o).getTemperature();
        tf.setText(s);
    }

}
