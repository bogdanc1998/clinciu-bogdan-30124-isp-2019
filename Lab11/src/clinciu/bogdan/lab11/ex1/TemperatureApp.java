/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinciu.bogdan.lab11.ex1;

import java.awt.BorderLayout;
import javax.swing.JFrame;

/**
 *
 * @author Bogdan
 */
public class TemperatureApp extends JFrame {

    public TemperatureApp(TextViewer t) {
        setLayout(new BorderLayout());
        add(t, BorderLayout.SOUTH);
        pack();
        setVisible(true);
    }

    public static void main(String[] args) {
        Senzor ts = new Senzor();
        ts.start();

        TextViewer tv = new TextViewer();
        Controller tc = new Controller(ts, tv);

        new TemperatureApp(tv);
    }
}
